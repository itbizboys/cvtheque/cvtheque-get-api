FROM golang:1.15

WORKDIR /go/src/app
COPY main.go .
RUN mkdir static

RUN go get -d -v ./...
RUN go build -v ./...

CMD ["./app"]
EXPOSE 3000