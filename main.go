package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Profile struct (Model)
type Profile struct {
	Sender     string `json:"sender"`
	City       string `json:"city"`
	Post       string `json:"post"`
	Languages  string `json:"languages"`
	Experience string `json:"experience"`
	Filename   string `json:"filename"`
}

type Resp struct {
	RecordsTotal    int       `json:"recordsTotal"`
	RecordsFiltered int       `json:"recordsFiltered"`
	Data            []Profile `json:"data"`
}

// Init profiles var as a slice Contact struct
var profiles []Profile
var Response Resp

func mongoCon() *mongo.Client {
	clientOptions := options.Client().ApplyURI("mongodb://root:Azer1234@cvtheque-db:27017/?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	// // Don't forget to logout
	// defer client.Disconnect(context.TODO())
	return client
}

var MongoClient = mongoCon()

func getProfilesDB() []Profile {
	profiles = nil
	collection := MongoClient.Database("cvtheque-db").Collection("profile")

	cursor, err := collection.Find(context.TODO(), bson.D{})
	if err != nil {
		fmt.Println(err)
	}

	defer cursor.Close(context.TODO())
	var profilesRaw []bson.M
	if err = cursor.All(context.TODO(), &profilesRaw); err != nil {
		fmt.Println(err)
	}

	// defer MongoClient.Disconnect(context.TODO())

	for i := 0; i < len(profilesRaw); i++ {
		profiles = append(profiles,
			Profile{
				Sender: profilesRaw[i]["from"].(string),
				City:   profilesRaw[i]["city"].(string),
				Post:   profilesRaw[i]["profile"].(string),
			})
	}

	// Response = Resp{RecordsTotal: len(profilesRaw), RecordsFiltered: len(profilesRaw), Data: profiles}
	return profiles
}

func getfProfilesDB(filter bson.D) []Profile {
	profiles = nil
	collection := MongoClient.Database("cvtheque-db").Collection("profile")

	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
	}

	defer cursor.Close(context.TODO())
	var profilesRaw []bson.M
	if err = cursor.All(context.TODO(), &profilesRaw); err != nil {
		fmt.Println(err)
	}

	// defer MongoClient.Disconnect(context.TODO())

	for i := 0; i < len(profilesRaw); i++ {
		if profilesRaw[i]["experience"] == nil {
			profilesRaw[i]["experience"] = ""
		}
		if profilesRaw[i]["languages"] == nil {
			profilesRaw[i]["languages"] = ""
		}

		profiles = append(profiles,
			Profile{
				Sender:     profilesRaw[i]["from"].(string),
				City:       profilesRaw[i]["city"].(string),
				Post:       profilesRaw[i]["profile"].(string),
				Languages:  profilesRaw[i]["languages"].(string),
				Experience: profilesRaw[i]["experience"].(string),
				Filename:   profilesRaw[i]["filename"].(string),
			})
	}

	// Response = Resp{RecordsTotal: len(profilesRaw), RecordsFiltered: len(profilesRaw), Data: profiles}

	return profiles
}

// Get all profiles
func getProfiles(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTIONS,DELETE,PUT")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

	// Response = getProfilesDB()
	json.NewEncoder(w).Encode(getProfilesDB())
}

// Get all profiles
func getFilteredProfiles(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTIONS,DELETE,PUT")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

	v := r.URL.Query()
	var filter bson.D

	if v.Get("ville") != "" {
		filter = append(filter, bson.E{"city", strings.ToLower(v.Get("ville"))})
	}

	if v.Get("post") != "" {
		filter = append(filter, bson.E{"profile", strings.ToLower(v.Get("post"))})
	}

	if v.Get("experience") != "" {
		filter = append(filter, bson.E{"experience", v.Get("experience")})
	}

	if v.Get("language") != "" {
		filter = append(filter,
			bson.E{"languages", bson.M{"$regex": primitive.Regex{".*" + v.Get("language") + ".*", "i"}}})
	}

	// Response = getfProfilesDB(filter)
	json.NewEncoder(w).Encode(getfProfilesDB(filter))
}

func noDirListing(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/") {
			http.NotFound(w, r)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// Main function
func main() {
	// Init router
	r := mux.NewRouter()

	// Route handles & endpoints
	r.HandleFunc("/profiles", getProfiles).Methods("GET")
	// http://localhost:3000/fprofiles?ville=casablanca&post=commercial
	r.HandleFunc("/fprofiles", getFilteredProfiles).Methods("GET")

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	// Start server
	log.Fatal(http.ListenAndServe(":3000", r))
}
